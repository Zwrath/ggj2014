﻿Shader "FullScreenOverlay"
{

    Properties
    {
        _Color ( "Color", Color ) = (1,1,1,1)
    }

    SubShader
    {
  
        Tags
        {
            "Queue" = "Overlay"
            "IgnoreProjector" = "True"
        }

        LOD 100

        Pass
        {
      
            Lighting Off
            Blend SrcAlpha OneMinusSrcAlpha	
            Cull Off
            ZWrite Off
			ZTest Off
          
            CGPROGRAM
          
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
          
            // user-defined variables
            uniform fixed4      _Color;
          
            struct vertexInput
            {
                float4 vertex   : POSITION;
            };
          
            struct vertexOutput
            {
                float4 pos      : SV_POSITION;
            };     
                      
            vertexOutput vert( vertexInput v )
            {
               
                vertexOutput o;
                o.pos = v.vertex;
                return o;
          
            }
          
            float4 frag( vertexOutput i ) : COLOR
            {
                return _Color;              
            }              
          
            ENDCG
      
        }
      
    }

}
