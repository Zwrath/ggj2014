﻿using UnityEngine;
using System.Collections;

public class Trigger : MonoBehaviour
{
	public GameObject activateObject;

	public void OnTriggerEnter2D(Collider2D c)
	{
		activateObject.SetActive(true);
		Destroy(gameObject);
	}
}
