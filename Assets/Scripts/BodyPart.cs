﻿using UnityEngine;
using System.Collections;

public class BodyPart : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
		m_particles = GetComponentInChildren<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		m_particles.emissionRate *= .99f;
		if (m_particles.emissionRate < 1.0f)
			Destroy(gameObject);
	}
	private ParticleSystem m_particles;
}
