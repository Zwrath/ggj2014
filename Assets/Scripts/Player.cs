using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Bunny))]
public class Player : MonoBehaviour
{

	void OnDestroy()
	{
		if(Game.Instance)
			Game.Instance.Restart();
	}

	void Start ()
	{
		m_controller = GetComponent<Bunny>().Controller = new BunnyController();
	}

	void Update()
	{
		m_controller.moveLeft = Input.GetKey(KeyCode.LeftArrow);
		m_controller.moveRight = Input.GetKey(KeyCode.RightArrow);
		m_controller.jump = Input.GetKey(KeyCode.UpArrow);

		if (Input.GetKeyDown(KeyCode.Space))
			Game.Rage = !Game.Rage;
	}

	private BunnyController m_controller;
}
