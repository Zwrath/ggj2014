using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Bunny))]
public class Enemy : MonoBehaviour
{

	public AudioClip[] ambientRageClips;
	public float ambientRageChance = 0.1f;
	public AudioClip[] rageGrowlClips;
	public float growlChance = 0.1f;
	public AudioClip[] giggleClips;
	public float giggleChance = 0.1f;

	void Start ()
	{
		m_controller = GetComponent<Bunny>().Controller = new BunnyController();

		ChangeState(Idle());
	
	}

	private void ChangeState(IEnumerator state)
	{
		StopAllCoroutines();
		StartCoroutine(state);
	}

	#region ==================================== States ====================================

	private IEnumerator Idle()
	{
		while (true)
		{
			if (Game.Rage)
				ChangeState(Rage());

			Util.PlayRandomClip(giggleClips, giggleChance, transform.position);
			
			int r = Random.Range(0, 8);
			m_controller.moveLeft = r == 0;
			m_controller.moveRight = r == 1;
			m_controller.jump = r == 2;
			yield return new WaitForSeconds(.1f + Random.value);
		}
	}

	private IEnumerator Rage()
	{
		while (true)
		{
			if (!Game.Rage)
				ChangeState(Idle());

			Util.PlayRandomClip(ambientRageClips, ambientRageChance, transform.position);
			Util.PlayRandomClip(rageGrowlClips, growlChance, transform.position);

			Bunny closestBunny = null;
			float closestDist = float.MaxValue;

			foreach (Bunny b in FindObjectsOfType<Bunny>())
			{
				if (b.gameObject == gameObject)
					continue;

				float d = (b.transform.position - transform.position).sqrMagnitude;
				if (d < closestDist)
				{
					closestDist = d;
					closestBunny = b;
				}
			}

			if (closestBunny)
			{
				Vector3 dif = closestBunny.transform.position - transform.position;
				m_controller.jump = dif.y > 0.1f || Random.value > 0.8f;

				bool rand = Random.value > 0.9f;
				m_controller.moveLeft = dif.x < 0.0f ^ rand;
				m_controller.moveRight = dif.x > 0.0f ^ rand;
			}

			
			yield return new WaitForSeconds(.1f + .5f*Random.value);
		}
	}

	#endregion


	private BunnyController m_controller;
}
