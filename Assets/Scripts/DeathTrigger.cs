using UnityEngine;
using System.Collections;

public class DeathTrigger : MonoBehaviour
{
	public void OnTriggerStay2D(Collider2D c)
	{
		Destroy(c.gameObject);
	}
}
