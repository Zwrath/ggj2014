using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour
{
	public float normalPitch = 1.0f;
	public float ragePitch = 0.5f;
	public float volume = 0.5f;

	void SetMusic(float p)
	{
		m_sources[0].volume = volume*(1.0f - p);
		m_sources[0].pitch = Mathf.Lerp(normalPitch, ragePitch, p);
		m_sources[1].volume = volume*p;
	}

	void Start()
	{
		m_sources = gameObject.GetComponents<AudioSource>();
		if (Game.Rage)
			m_music = 1;
		else
			m_music = 0;
		SetMusic(m_music);
	}

	void Update()
	{
		if (Game.Rage)
			m_music = Mathf.MoveTowards(m_music, 1, Time.deltaTime / Game.Instance.rageFadeTime);
		else
			m_music = Mathf.MoveTowards(m_music, 0, Time.deltaTime / Game.Instance.rageFadeTime);
		SetMusic(m_music);
	}

	private AudioSource[] m_sources;

	float m_music;
}
