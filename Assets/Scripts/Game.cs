using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour
{
	public float rageDelay;
	public float rageFadeTime;

	public static Game Instance
	{
		get
		{
			if (!m_instance)
				m_instance = GameObject.FindObjectOfType<Game>();
			return m_instance;
		}
	}

	private static Game m_instance;

	void Start()
	{
		m_player = FindObjectOfType<Player>();
		if (!m_player)
			Debug.LogError("no player found");
	}

	public void Restart()
	{
		if (!Application.isLoadingLevel)
		{
			StartCoroutine(restart());
		}
	}

	IEnumerator restart()
	{
		yield return new WaitForSeconds(2.0f);
		Application.LoadLevel(Application.loadedLevel);
		Rage = false;
	}

	public static bool Rage
	{
		get
		{
			return rage;
		}
		set
		{
			if (value != rage)
			{
				if (Application.loadedLevel > 1)
				{
					rage = value;
					SendMessage("RageChanged");
				}
			}
		}
	}

	private static void SendMessage(string msg)
	{
		GameObject dynamic = GameObject.Find("Dynamic");
		dynamic.BroadcastMessage(msg, null, SendMessageOptions.DontRequireReceiver);
		GameObject persistent = GameObject.Find("Persistent");
		persistent.BroadcastMessage(msg, null, SendMessageOptions.DontRequireReceiver);
	}

	private static bool rage;

	private Player m_player;
}
