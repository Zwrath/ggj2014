using UnityEngine;
using System.Collections;

public class DynamicObject : MonoBehaviour
{
	public AudioClip[] collisionClips;

	void OnCollisionEnter2D(Collision2D c)
	{
		Util.PlayRandomClip(collisionClips, 1.0f, c.contacts[0].point, .5f * c.relativeVelocity.magnitude);
	}

	void Start()
	{
		m_originalMass = rigidbody2D.mass;
		RageChanged();
	}
	public void RageChanged()
	{
		if(Game.Rage)
			rigidbody2D.mass = m_originalMass;
		else
			rigidbody2D.mass = m_originalMass * 100;
	}

	private float m_originalMass;
}
