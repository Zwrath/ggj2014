using UnityEngine;
using System.Collections;

public class BunnyController
{
	public bool moveLeft;
	public bool moveRight;
	public bool jump;
}

[RequireComponent(typeof(CircleCollider2D))]
public class Bunny : MonoBehaviour
{

	[System.Serializable]
	public class BunnyStats
	{
		public float moveSpeed = 1.0f;
		public float moveAcceleration = 1.0f;
		public float airAcceleration = 1.0f;
		public float jumpVelocity = 10.0f;
		public float jumpTime = 0.5f;
		public AudioClip[] jumpClips;
		public AudioClip[] deathClips;
		public Material material;
		public Material eyeMaterial;
		
		public GameObject bunnyModel;
		public GameObject head;
	}
	public BunnyStats normalStats;
	public BunnyStats rageStats;

	public float grappleJumpSpeedScale = 0.5f;

	public AudioClip[] gutsClips;
	public AudioClip[] footClips;
	public float footstepInterval = 0.4f;
	public AudioClip[] dragClips;

	public GameObject[] bodyParts;

	public BunnyController Controller
	{
		set
		{
			m_controller = value;
		}
	}


	#region EVENTS

	public void OnCollisionEnter2D (Collision2D coll)
	{
		if (1 << coll.gameObject.layer == m_dynamicLayer)
		{
			m_collidedWithDynamicObject = 0.1f;
		}
	}

	void OnCollisionStay2D (Collision2D collision)
	{
		if (m_currentStats == rageStats && ((1 << collision.gameObject.layer) & m_bunnyLayer) != 0)
		{
			if (GetComponent<Player> ())
			{
				Destroy (gameObject);
			}
			else
			if (Random.value > 0.9f)
			{
				Destroy (gameObject);
			}
		}
	}

	public void OnDestroy ()
	{
		if (m_bloodParticles.gameObject.activeInHierarchy)
		{
			m_bloodParticles.Emit (transform.position, 100);
			m_bloodParticles.Destroy ();
			Util.PlayRandomClip (m_currentStats.deathClips, 1.0f, transform.position, 0.65f);
			Util.PlayRandomClip (gutsClips, 1.0f, transform.position, 1.0f);

			GameObject partBlood = Resources.Load<GameObject>("Effects/PartBlood");
			foreach (GameObject part in bodyParts)
			{
				// No eyes in rage mode
				if (m_currentStats == rageStats && part.name == "BunnyEye")
					continue;
				
				GameObject o = (GameObject)Instantiate(part, transform.position, part.transform.rotation);
				o.rigidbody2D.velocity = 20*new Vector2(Random.value - .5f, Random.value - .5f);
				o.rigidbody2D.angularVelocity = 5 * (Random.value - .5f);
				SetMaterial(o.GetComponentInChildren<MeshRenderer>());

				GameObject blood = (GameObject)Instantiate(partBlood, transform.position, part.transform.rotation);
				blood.transform.parent = o.transform;
				blood.transform.localPosition = Vector3.zero;

			}

			GameObject o2 = (GameObject)Instantiate(m_currentStats.head, transform.position, m_currentStats.head.transform.rotation);
			o2.rigidbody2D.velocity = 20 * new Vector2(Random.value - .5f, Random.value - .5f);
			o2.rigidbody2D.angularVelocity = 5 * (Random.value - .5f);
			SetMaterial(o2.GetComponentInChildren<MeshRenderer>());

			GameObject blood2 = (GameObject)Instantiate(partBlood, transform.position, Quaternion.identity);
			blood2.transform.parent = o2.transform;
			blood2.transform.localPosition = Vector3.zero;
		}
	}

	public void RageChanged ()
	{
		if (Game.Rage)
		{
			StartCoroutine (startRage ());
		}
		else
		{
			StartCoroutine (endRage ());
		}
	}

	#endregion

	#region MONOBEHAVIOUR

	void Start ()
	{
		m_body = rigidbody2D;
		m_collider = GetComponent<CircleCollider2D> ();
		m_groundLayer = 1 << LayerMask.NameToLayer ("Ground");
		m_dynamicLayer = 1 << LayerMask.NameToLayer ("DynamicObject");
		m_bunnyLayer = (1 << LayerMask.NameToLayer ("Player")) | (1 << LayerMask.NameToLayer ("Enemy"));

		GameObject blood = (GameObject)Instantiate (Resources.Load<GameObject> ("Effects/Blood"), transform.position, transform.rotation);
		m_bloodParticles = blood.GetComponent<Particles> ();
		
		GameObject dust = (GameObject)Instantiate (Resources.Load<GameObject> ("Effects/Dust"), transform.position, transform.rotation);
		m_dustParticles = dust.GetComponent<Particles> ();

		normalStats.bunnyModel = (GameObject)Instantiate (normalStats.bunnyModel, transform.position, transform.rotation);
		normalStats.bunnyModel.transform.parent = transform;
		normalStats.bunnyModel.transform.localPosition = new Vector3 (0, -.5f, 0);
		normalStats.bunnyModel.transform.localRotation = Quaternion.Euler (0, 180, 0);

		rageStats.bunnyModel = (GameObject)Instantiate (rageStats.bunnyModel, transform.position, transform.rotation);
		rageStats.bunnyModel.transform.parent = transform;
		rageStats.bunnyModel.transform.localPosition = new Vector3 (0, -.5f, 0);
		rageStats.bunnyModel.transform.localRotation = Quaternion.Euler (0, 180, 0);

		m_currentStats = normalStats;
		m_bunnyModel = m_currentStats.bunnyModel;
		m_animation = m_currentStats.bunnyModel.animation;
		rageStats.bunnyModel.SetActive (false);

		SetMaterial(m_bunnyModel.GetComponentInChildren<SkinnedMeshRenderer>());
		RageChanged ();
	}

	
	void FixedUpdate ()
	{
		Vector2 vel = m_body.velocity;

		bool moveLeft = false;
		bool moveRight = false;
		bool jump = false;

		if (m_controller != null && !m_changeInProgress)
		{
			moveLeft = m_controller.moveLeft;
			moveRight = m_controller.moveRight;
			jump = m_controller.jump;
		}

		// X movement
		float move = 0;
		if (moveLeft)
		{
			move -= m_currentStats.moveSpeed;
		}
		if (moveRight)
		{
			move += m_currentStats.moveSpeed;
		}

		// Jump
		Collider2D ground = checkWall (new Vector2 (0, -.1f), .9f);
		Collider2D right = checkWall (new Vector2 (.3f, 0), .5f);
		Collider2D left = checkWall (new Vector2 (-.3f, 0), .5f);
		Collider2D up = checkWall (new Vector2 (0, .1f), .9f);

		if (ground)
		{
			vel.x = Mathf.MoveTowards (vel.x, move, m_currentStats.moveAcceleration);
		}
		else
		{
			vel.x = Mathf.MoveTowards (vel.x, move, m_currentStats.airAcceleration);
		}

		bool grappleLeft = false;
		bool grappleRight = false;

		bool prevJumping = m_jumping;

		if (ground)
		{
			if (jump)
			{
				m_jumping = true;
				m_jumpTimer = m_currentStats.jumpTime;
			}
			m_body.gravityScale = 0.0f;
		}
		// Grapple only in rage mode
		else
		if (!up && Game.Rage)
		{
			if (left && moveLeft)
			{
				vel.y = 0;
				grappleLeft = true;
				m_body.gravityScale = 0.0f;
				if (jump)
				{
					m_jumping = true;
					m_jumpTimer = m_currentStats.jumpTime;
					vel.x = m_currentStats.moveSpeed * grappleJumpSpeedScale;
					m_collidedWithDynamicObject = 0.0f;
				}
			}
			else
			if (right && moveRight)
			{
				vel.y = 0;
				grappleRight = true;
				m_body.gravityScale = 0.0f;
				if (jump)
				{
					m_jumping = true;
					m_jumpTimer = m_currentStats.jumpTime;
					vel.x = -m_currentStats.moveSpeed * grappleJumpSpeedScale;
					m_collidedWithDynamicObject = 0.0f;
				}
			}
			else
			{
				m_body.gravityScale = 1.0f;
			}
		}
		else
		{
			m_body.gravityScale = 1.0f;
		}

		if (!jump || up)
		{
			m_jumping = false;
		}

		if (m_jumping && jump)
		{
			vel.y = m_currentStats.jumpVelocity;

			m_jumpTimer -= Time.deltaTime;
			if (m_jumpTimer <= 0)
			{
				m_jumping = false;
			}
		}

		// Started to jump
		if (!prevJumping && m_jumping)
		{
			Util.PlayRandomClip (m_currentStats.jumpClips, 1.0f, transform.position, 0.65f);
		}

		// Animations
		if (ground)
		{
			if ((vel.x > 0.1f && !right) || (vel.x < -0.1f && !left))
			{
				if (m_bunnyModel == normalStats.bunnyModel)
				{
					m_animation.CrossFade ("Walk", .1f);
					m_animation ["Walk"].speed = Mathf.Abs (vel.x) * .8f;
				}
				else
				{
					m_animation.CrossFade ("Run", .1f);
					m_animation ["Run"].speed = Mathf.Abs (vel.x) * .5f;
				}

				m_footstepTimer -= Mathf.Abs (vel.x) * Time.deltaTime;
				if (m_footstepTimer <= 0)
				{
					m_footstepTimer = footstepInterval;

					float footStepVolume = 0.5f;
					if (Game.Rage)
					{
						footStepVolume = 1.0f;
					}

					Util.PlayRandomClip (footClips, 1.0f, transform.position, footStepVolume);

					m_dustParticles.Emit (transform.position + Vector3.down * .1f, 5);
				}
			}
			else
			{
				if (m_bunnyModel == rageStats.bunnyModel && ((vel.x > 0.1f && right) || (vel.x < -0.1f && left)))
				{
					m_animation ["Push"].speed = Mathf.Abs (vel.x) * 2.0f;
					m_animation.CrossFade ("Push", .1f);
					vel.y -= .2f;
					if(Mathf.Abs(vel.x) > 0.2f)
						Util.PlayRandomClip(dragClips, .5f, transform.position, 1.0f);
				}
				else
				{
					m_animation.CrossFade ("Idle", .2f);
				}
			}
		}
		else
		{
			if (m_bunnyModel == rageStats.bunnyModel && (grappleLeft || grappleRight))
			{
				m_animation.CrossFade ("WallGrab", .1f);
			}
			else
			if (vel.y > 0)
			{
				m_animation.CrossFade ("JumpUp", .1f);
			}
			else
			{
				m_animation.CrossFade ("JumpDown", .3f);
			}
		}

		Quaternion targetRot;
		if (left)
		{
			targetRot = Quaternion.Euler (0, 0, 0);
		}
		else
		if (right)
		{
			targetRot = Quaternion.Euler (0, 180, 0);
		}
		else
		{
			targetRot = Quaternion.Euler (0, Mathf.Clamp (270 - 90 * vel.x, 180, 360), 0);
		}
		m_bunnyModel.transform.localRotation = Quaternion.Slerp (m_bunnyModel.transform.localRotation, targetRot, Time.deltaTime * 10);

		m_body.velocity = vel;
	}

	void LateUpdate ()
	{
		if (m_collidedWithDynamicObject > 0 && !Game.Rage)
		{
			// if velocity changed in X-direction alot it indicates that the body was squished under dynamic object
			if (Mathf.Abs (m_body.velocity.x - m_lastVelocity.x) > 3.0f)
			{
				transform.position = m_lastPosition;
				Destroy (gameObject);
				return;
			}

			m_collidedWithDynamicObject -= Time.deltaTime;
		}

		m_lastVelocity = m_body.velocity;
		m_lastPosition = transform.position;
	}

	#endregion

	#region COROUTINES

	IEnumerator startRage ()
	{
		m_changeInProgress = true;

		yield return new WaitForSeconds (Game.Instance.rageDelay);

		m_currentStats = rageStats;

		m_bunnyModel.SetActive (false);
		m_bunnyModel = m_currentStats.bunnyModel;
		m_animation = m_currentStats.bunnyModel.animation;
		m_bunnyModel.SetActive (true);
		SetMaterial (m_bunnyModel.GetComponentInChildren<SkinnedMeshRenderer>());

		m_changeInProgress = false;
		yield break;
	}

	IEnumerator endRage ()
	{
		m_changeInProgress = true;

		yield return new WaitForSeconds (Game.Instance.rageDelay);
		m_currentStats = normalStats;

		m_bunnyModel.SetActive (false);
		m_bunnyModel = m_currentStats.bunnyModel;
		m_animation = m_currentStats.bunnyModel.animation;
		m_bunnyModel.SetActive (true);
		SetMaterial(m_bunnyModel.GetComponentInChildren<SkinnedMeshRenderer>());

		m_changeInProgress = false;
		yield break;
	}
	
	#endregion

	#region PRIVATE FUNCTIONS

	private void SetMaterial (Renderer r)
	{
		Material[] mats = r.sharedMaterials;
		mats [0] = m_currentStats.material;
		if (mats.Length > 1)
		{
			mats [1] = m_currentStats.eyeMaterial;
		}
		r.sharedMaterials = mats;
	}

	Collider2D checkWall (Vector2 offset, float radiusFactor)
	{
		Vector3 pos = transform.position;
		Vector2 pos2D = new Vector2 (pos.x, pos.y);

		offset.x *= transform.localScale.x;
		offset.y *= transform.localScale.y;
		return Physics2D.OverlapCircle (pos2D + offset, Util.WorldRadius (m_collider) * radiusFactor, m_groundLayer | m_dynamicLayer);
	}

	#endregion

	#region PRIVATE VARIABLES

	private LayerMask m_groundLayer;
	private LayerMask m_dynamicLayer;
	private LayerMask m_bunnyLayer;

	private Rigidbody2D m_body;
	private CircleCollider2D m_collider;
	private BunnyController m_controller;
	private Particles m_bloodParticles;
	private Particles m_dustParticles;
	
	private bool m_jumping;
	private float m_jumpTimer;

	private BunnyStats m_currentStats;

	private Vector2 m_lastVelocity;
	private Vector3 m_lastPosition;
	private float m_collidedWithDynamicObject;

	private GameObject m_bunnyModel;
	private Animation m_animation;
	private bool m_changeInProgress;

	private float m_footstepTimer;


	#endregion
}
