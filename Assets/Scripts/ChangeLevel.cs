using UnityEngine;
using System.Collections;

public class ChangeLevel : MonoBehaviour
{
	public string level;

	void OnTriggerEnter2D(Collider2D coll)
	{
		Application.LoadLevel(level);
		Game.Rage = false;
	}
}
