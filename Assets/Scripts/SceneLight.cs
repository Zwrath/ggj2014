using UnityEngine;
using System.Collections;

public class SceneLight : MonoBehaviour
{
	public Color normalColor;
	public Color rageColor;

	public Color normalAmbientLight;
	public Color rageAmbientLight;

	void SetStuff(float rage)
	{
		light.color = Color.Lerp(normalColor, rageColor, rage);
		light.shadowStrength = Mathf.Lerp(.5f, 1.0f, rage);
		RenderSettings.ambientLight = Color.Lerp(normalAmbientLight, rageAmbientLight, rage);

	}

	void Start()
	{
		m_rage = 0.0f;
	}

	void Update()
	{
		if (Game.Rage)
		{
			m_rage = Mathf.MoveTowards(m_rage, 1.0f, Time.deltaTime / Game.Instance.rageFadeTime);
			SetStuff(m_rage);
		}
		else
		{
			m_rage = Mathf.MoveTowards(m_rage, 0.0f, Time.deltaTime / Game.Instance.rageFadeTime);
			SetStuff(m_rage);
		}
		
	}

	float m_rage;
}
