using UnityEngine;
using System.Collections;

public class Util
{
	public static float WorldRadius(CircleCollider2D collider)
	{
		Vector3 s = collider.transform.lossyScale;
		return Mathf.Max(s.x, s.y) * collider.radius;
	}
	
	public static void PlayRandomClip(AudioClip[] clips, float chance, Vector3 position, float volume = 1.0f)
	{
		if (clips.Length == 0)
			return;
		if (Random.value < chance)
			AudioSource.PlayClipAtPoint(clips[Random.Range(0, clips.Length)], position, volume);
	}
}
