using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class FullScreenOverlay : MonoBehaviour
{
	public Color rageFlashColor;
	public Color normalFlashColor;

	public Material normalSky;
	public Material rageSky;
	public Material groundMaterial;
	public Material rockMaterial;
	public Texture2D normalGroundTexture;
	public Texture2D normalRockTexture;
	public Texture2D evilGroundTexture;
	public Texture2D evilRockTexture;

	void Start ()
	{
		CreateMesh();
		groundMaterial.mainTexture = normalGroundTexture;
		rockMaterial.mainTexture = normalRockTexture;
	}

	public void RageChanged()
	{
		StartCoroutine(changeRage(Game.Rage));
	}

	IEnumerator changeRage(bool rage)
	{
		Color flashColor;
		if (rage)
			flashColor = rageFlashColor;
		else
			flashColor = normalFlashColor;

		yield return new WaitForSeconds(Game.Instance.rageDelay - .25f * Game.Instance.rageFadeTime);

		float t = 0.0f;
		while (t < 1.0f)
		{
			float p;
			if (t < .25f)
				p = t * 4;
			else
			{
				p = 1.0f - Mathf.Abs(Mathf.Sin((t * .75f + .75f) * Mathf.PI));
				if (rage)
				{
					groundMaterial.mainTexture = evilGroundTexture;
					rockMaterial.mainTexture = evilRockTexture;
					RenderSettings.skybox = rageSky;
				}
				else
				{
					groundMaterial.mainTexture = normalGroundTexture;
					rockMaterial.mainTexture = normalRockTexture;
					RenderSettings.skybox = normalSky;
				}
			}

			renderer.material.color = Color.Lerp(new Color(0, 0, 0, 0), flashColor, p);

			t += Time.deltaTime / Game.Instance.rageFadeTime;
			yield return null;
		}
		yield break;
	}

	void CreateMesh()
	{
		Mesh mesh = GetComponent<MeshFilter>().mesh;

		mesh.vertices = new Vector3[4]
		{
			new Vector3(-10.1f,-10.1f),
			new Vector3( 10.1f,-10.1f),
			new Vector3( 10.1f, 10.1f),
			new Vector3(-10.1f, 10.1f),
		};

		mesh.triangles = new int[6]
		{
			0, 1, 2,
			2, 3, 0
		};

		renderer.material.shader = Shader.Find("FullScreenOverlay");
		renderer.material.color = new Color(0, 0, 0, 0);
	}
}
