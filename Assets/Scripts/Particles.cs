using UnityEngine;
using System.Collections;

public class Particles : MonoBehaviour
{
	public void Emit(Vector3 position, int count)
	{
		transform.position = position;
		particleSystem.Emit(count);
	}

	public void Start()
	{
		transform.parent = GameObject.Find("Dynamic").transform;
	}

	public void Destroy()
	{
		StartCoroutine(destruction());
	}

	IEnumerator destruction()
	{
		particleSystem.Stop();
		while (particleSystem.IsAlive())
		{
			yield return null;
		}
		Destroy(gameObject);
		yield break;
	}

}
