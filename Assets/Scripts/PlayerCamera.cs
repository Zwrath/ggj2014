using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{
	public float distance = 10.0f;
	public Rect cameraArea;

	public Color normalBGColor;
	public Color rageBGColor;
	public Color normalFogColor;
	public Color rageFogColor;

	public bool enablePS = true;

	public float rageFOVFactor = 2.0f;
	public float upOffset = 0.0f;

	public Vector2 velocityOffset;

	void Start ()
	{
		m_followPlayer = FindObjectOfType<Player>();
		m_followTransform = m_followPlayer.transform;

		if (!enablePS)
		{
			GetComponent<SSAOEffect>().enabled = false;
			GetComponent<AmbientObscurance>().enabled = false;
			GetComponent<MotionBlur>().enabled = false;
		}
		m_normalFOV = camera.fieldOfView;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (!m_followTransform)
			return;
		Vector3 pos = transform.position;

		Vector2 vel = m_followPlayer.rigidbody2D.velocity;
		Vector3 targetPos = m_followTransform.position + Vector3.up * upOffset + new Vector3(velocityOffset.x * vel.x, velocityOffset.y * vel.y, 0);

		pos.z = targetPos.z;
		pos = Vector3.Lerp(pos, targetPos, .1f);
		pos.z = Mathf.Lerp(-distance, -distance / rageFOVFactor, m_rageLerp); ;
		

		if (pos.x < cameraArea.x)
			pos.x = cameraArea.x;

		pos.x = Mathf.Clamp(pos.x, cameraArea.x, cameraArea.x + cameraArea.width);
		pos.y = Mathf.Clamp(pos.y, cameraArea.y, cameraArea.y + cameraArea.height);

		transform.position = pos;

		if (Game.Rage)
		{
			m_rageLerp = Mathf.MoveTowards(m_rageLerp, 1.0f, Time.deltaTime / Game.Instance.rageFadeTime);
			if(enablePS)
				GetComponent<MotionBlur>().enabled = true;

		}
		else
		{
			m_rageLerp = Mathf.MoveTowards(m_rageLerp, 0.0f, Time.deltaTime / Game.Instance.rageFadeTime);
			GetComponent<MotionBlur>().enabled = false;
		}
		camera.backgroundColor = Color.Lerp(normalBGColor, rageBGColor, m_rageLerp);
		camera.fieldOfView = Mathf.Lerp(m_normalFOV, m_normalFOV * rageFOVFactor, m_rageLerp);
		RenderSettings.fogColor = Color.Lerp(normalFogColor, rageFogColor, m_rageLerp); 
	}

	private Player m_followPlayer;
	private Transform m_followTransform;
	private float m_rageLerp;

	private float m_normalFOV;
}
